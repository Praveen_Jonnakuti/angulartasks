import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backendservice.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error: boolean = false;
  errorMessage: String = "";
  private querySubscription;

  constructor(private _backendService: BackendService, private _route: Router) { }

  ngOnInit() {
  }

  login(formData){

    this.querySubscription = this._backendService.login(formData).subscribe((res) => {
        if (res["errorCode"] > 0) {
          this.error = false;
          this.errorMessage = "";
          window.localStorage.setItem('token', res["data"].token);
          this._route.navigate(['']);
      } else {
          this.error = true;
          this.errorMessage = res["errorMessage"];
      }
  },
      (error) => {
          this.error = true;
          this.errorMessage = error.message;
      },
  
      );
}

ngOnDestroy(){
  if (this.querySubscription) {
    this.querySubscription.unsubscribe();
}
}

        }