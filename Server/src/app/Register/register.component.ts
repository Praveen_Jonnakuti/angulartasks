import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backendservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  error: boolean = false;
  errorMessage: String = "";
  private querySubscription;
  savedChanges: boolean = false;


  constructor(private _backendService: BackendService, private _route: Router) { }

  ngOnInit() {
  }

  setUser(formData){
    this.querySubscription = this._backendService.setUser(formData).subscribe((res) => {
      if (res["errorCode"] > 0) {
          this.error = false;
          this.errorMessage = "";
          this.savedChanges = true;
      } else {
          this.error = true;
          this.errorMessage =res["errorMessage"];
        
      }
  },
      (error) => {
          this.error = true;
          this.errorMessage = error.message;
        
      },
     
      );
}

ngOnDestroy(){
  if (this.querySubscription) {
    this.querySubscription.unsubscribe();
}
}

}